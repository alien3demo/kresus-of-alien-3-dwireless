Title: Kresus - A libre personal finance manager
Date: 2019-06-17 18:34:00
Slug: home
Summary: home
lang: en
save_as: index.html
status: hidden
Template: home

<div class="features" id="firstFeature">
    <div>
        <div>
            <h3>All your accounts in a glance!</h3>
            <p>
                Find all your accounts in a common interface and check
                their balance in a glance!
            </p>
        </div>

        <p class="screenshot" id="feature-accounts">
            <a href="#feature-accounts-closed" id="feature-accounts-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-accounts">
                <img src="/images/pages/view-all-accounts.png" alt="Global UI screenshot" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3>Master your budget!</h3>
            <p>
                You can easily configure <strong>email alerts</strong>
                when an operation's amount is over a threshold  or on your account balance to
                <strong>warn</strong> you in case of <strong>major
                    events</strong> on your accounts.
            </p>
        </div>

        <p class="screenshot" id="feature-budget">
            <a href="#feature-budget-closed" id="feature-budget-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-budget">
                <img src="/images/pages/budget.png" alt="Monthly view screenshot" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3>Sort, rename and find your operations!</h3>
            <ul>
                <li>Set your own <strong>labels</strong>, farewell to "<em>Check n°168468</em>"!</li>
                <li><strong>Sort</strong> your operations by <strong>categories</strong></li>
                <li><strong>Search</strong> within your transactions,
                    by time period, category, amount or date!</li>
            </ul>
        </div>

        <p class="screenshot" id="feature-categories">
            <a href="#feature-categories-closed" id="feature-categories-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-categories">
                <img src="/images/pages/categories.png" alt="Categories view screenshot" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3>Charts!</h3>
            <p>
                Track your operations in a glance with per categories
                charts, per period charts and per operation type
                (income / outcome) charts.
            </p>
        </div>

        <p class="screenshot" id="feature-charts">
            <a href="#feature-charts-closed" id="feature-charts-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-charts">
                <img src="/images/pages/charts.png" alt="Charts view screenshot" />
            </a>
        </p>
    </div>
</div>

<div class="features">
    <div>
        <div>
            <h3><em>Libre</em>, self-hostable and personal!</h3>
            <p>
                Who would feel comfortable sharing their banking
                credentials online with perfect strangers?
            </p>

            <p>
                Banking data is <strong>extremely personal</strong>.
                It reflects entire parts of our daily and intimate
                life: what we buy, who we are, what we do.
            </p>

            <p>
                Because nobody else than you should know about these data,
                Kresus is a <strong><em>libre</em></strong>,
                <strong>open-source</strong> and
                <strong>self-hostable</strong> software.
            </p>

            <p>
                To ensure you get the most accurate and up to date
                information automatically, Kresus uses another
                <em>libre</em> and active project, <a
                href="http://woob.tech/">Woob</a>, which takes care
                of scrapping data from your banking website.  </p>

            <p> <strong>With Kresus, you are in total control over
                both the software and your data!</strong>
            </p>
        </div>

        <p class="screenshot" id="feature-libre">
            <a href="#feature-libre-closed" id="feature-libre-closed" class="close">
                <span class="fa fa-times"></span>
            </a>
            <a href="#feature-libre">
                <img src="/images/pages/framagit.png" alt="Framagit screenshot" />
            </a>
        </p>
    </div>
</div>


