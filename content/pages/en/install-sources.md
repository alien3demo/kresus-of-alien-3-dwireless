Title: Install from the source code
Date: 2019-01-02 16:30
Slug: install-sources
lang: en
status: hidden

To install node dependencies and compile the scripts (this will not install Kresus globally) for a
production use:

    :::bash
    npm install && npm run build:prod

You will then be able to launch Kresus with

    :::bash
    NODE_ENV=production npm run start
