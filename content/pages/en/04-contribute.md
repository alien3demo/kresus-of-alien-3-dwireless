Title: How to contribute to Kresus
Date: 2017-03-11 10:02
Slug: contribute
Summary: Discover how to contribute to Kresus: code, doc, design, translations…
lang: en
is_doc: true
status: hidden

Kresus is a <a
href="https://framagit.org/kresusapp/kresus/blob/main/LICENSE"><strong><em>libre</em></strong></a>
and <a
href="https://framagit.org/kresusapp/kresus"><strong>open-source</strong></a>
software.
You can then contribute easily, whatever your skills!

Here are some ideas to get you started:

* Report any issue you might find in our [bugs
  tracker](https://framagit.org/kresusapp/kresus/issues).
* Choose an ["easy
  issue"](https://framagit.org/kresusapp/kresus/issues?label_name%5B%5D=easy)
  and fix it. These
  [guidelines](https://framagit.org/kresusapp/kresus/blob/main/CONTRIBUTING.md)
  to contribute might be useful.
* Enhance design or user interface of Kresus.
* [Translate Kresus](https://framagit.org/kresusapp/kresus/tree/main/shared/locales) in other languages.
* Write some documentation (this website is <em>open-source</em> too: [https://framagit.org/kresusapp/kresus.org](https://framagit.org/kresusapp/kresus.org) !)
* And simply talk about Kresus around you!

<em>Note:</em> Kresus contributors have set up a code of conduct so that
everyone is welcome to contribute. You should [read
it](https://framagit.org/kresusapp/kresus/blob/main/CodeOfConduct.md) first.
