Title: Installation
Date: 2017-03-11 10:02
Slug: install
Summary: Installez Kresus de toutes les manières possibles !
lang: fr
status: hidden
is_doc: true

Kresus peut être installé selon la manière qui vous convient le mieux :

- via [npm]({filename}install-npm.md),
- via [Docker]({filename}install-docker.md),
- via [Yunohost](https://yunohost.org), avec une
  [application](https://github.com/YunoHost-Apps/kresus_ynh) de niveau 7,
- sur ArchLinux, en utilisant le [package
  pacman](https://www.archlinux.org/packages/community/x86_64/kresus/): `pacman
  -Syu kresus`
    - *⚠︎ Ce package étant maintenu par la commuauté, il pourrait ne pas être
      aligné sur la dernière version de Kresus.*
- depuis [les sources]({filename}install-sources.md)

## Tutoriels de la communauté

Remercions tous les membres de la communauté pour tous les tutoriels complets
et très bien documentés expliquant comment installer Kresus dans des conditions
particulières !

Certains de ces tutoriels sont maintenus par la communauté et pourraient ne pas
être à jour. Nous vous conseillons d'utiliser la documentation de ce site, qui
sera maintenue à jour autant que possible.

- [fr] [Gérer ses finances avec Kresus](https://wiki.bruno-tatu.com/informatique/install-kresus)
- [fr] [Installer Kresus sur Debian 8](https://carmagnole.ovh/tuto-installer-kresus.htm)
- [fr] [Kresus, un gestionnaire Web de finances personnelles](https://blog.karolak.fr/2016/03/18/kresus-un-gestionnaire-web-de-finances-personnelles/)
- [fr] [Installer Kresus sur un Raspberry Pi 3](https://forum.cabane-libre.org/topic/525/installer-kresus-sur-un-raspberry-3)
- [fr] [Installer Kresus dans une jail FreeBSD](https://wiki.kingpenguin.tk/informatique/serveur/kresus)

Si vous écrivez un tutoriel lié à l'installation ou l'utilisation de Kresus,
n'hésitez-pas à nous contacter et nous serons ravis de l'inclure dans cette
liste !
