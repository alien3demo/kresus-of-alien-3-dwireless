Title: Administration
Date: 2020-01-02 16:30
Summary: Le guide d'administration système de Kresus, de la configuration aux backups.
Slug: admin
toc_run: true
toc_title: Administration d'une instance
lang: fr
status: hidden
is_doc: true

## Configuration

La toute première chose à faire avant d'utiliser Kresus est de le configurer,
afin qu'il puisse fonctionner de manière optimale selon vos préférences !

### Avec un fichier `config.ini`

Générez d'abord une configuration vide avec la commande ci-dessous.

    :::bash
    ./bin/kresus.js 'create:config' > config.ini

> ⚠️ **Note**: pour les versions précédant la 0.16 cette commande n'existe pas. Vous pouvez néanmoins trouver la liste des options disponibles dans le fichier [config.example.ini](https://framagit.org/kresusapp/kresus/raw/main/support/docker/config.example.ini).

Modifiez ensuite le fichier `config.ini` en définissant les options qui vous sont propres et passez
ensuite l'argument `-c path/to/config.ini` à Kresus au lancement.

**Sécurité :** En mode production (`NODE_ENV=production`), si le fichier de
configuration ne fournit pas uniquement les droits de lecture ou
lecture/écriture à son propriétaire, en utilisant les permissions du système
de fichier, Kresus refusera de démarrer.

### Avec des variables d'environnement

Notez que chaque option de configuration a une variable d'environnement
associée : si la variable d'environnement est définie, elle surchargera la
configuration du fichier `INI` ou la valeur par défaut. Référez-vous au
[fichier
`config.ini.example`](https://framagit.org/kresusapp/kresus/raw/main/support/docker/config.example.ini)
pour trouver toutes les variables d'environnement utilisables.

## Base de données SQL

À partir de la version 0.16 incluse, il est nécessaire de fournir un fichier de
configuration à Kresus pour définir quel système de base de données sera
utilisé. Nous vous invitons à vous référer au [fichier
exemple](https://framagit.org/kresusapp/kresus/raw/main/support/docker/config.example.ini) pour découvrir les options associées à la base de données.

La seule base de données compatible avec Kresus et recommandée en production
est [PostgreSQL](https://www.postgresql.org/).

Il est **fortement recommandé d'éviter** `sqlite` en production. `sqlite` ne
supporte pas très bien les migrations de structures et de données. Nous
recommandons uniquement l'utilisation de `sqlite` à des fins de tests et de
développement.

Si vous décidez d'utiliser `sqlite`,  il vous faudra installer le paquet
`sqlite3`: `npm install --save sqlite3`.

### Migrer depuis l'ancien système vers SQL

Avant la version 0.16, Kresus utilisait un autre système de base de données sur
disque peu performant. Le passage vers une base de données SQL permet
d'améliorer généralement les performances et d'ouvrir de nouveaux horizons pour
de nouvelles fonctionnalités.

Il est conseillé d'effectuer un [backup](#backup) de votre instance avant
d'effectuer la migration vers le système SQL.

#### Depuis la version 0.15 vers la 0.16

Si vous utilisiez la version 0.15 et êtes passé à la version 0.16, alors la
migration devrait se dérouler automatiquement au démarrage de Kresus, une seule
fois (si elle réussit). Vos logs devraient vous informer si cette migration a
bien eu lieu ou non ; si ce n'est pas le cas, merci de nous [signaler le
souci](https://framagit.org/kresusapp/kresus/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

#### Depuis une version antérieure à 0.14 incluse vers la 0.16

Si vous utilisiez une version antérieure à la version 0.15 avant le passage
vers SQL (par exemple : vous utilisez la version 0.14 et passez directement à
la 0.16), alors vous pouvez exporter toutes vos données grâce à l'outil
`kresus-export-cozydb` :

- installez cet outil :

        :::bash
        npm install -g kresus-export-cozydb


- lancez-le en référant à votre ancienne configuration, avant SQL :

        :::bash
        kresus-export-cozydb --config /path/to/old/config.ini --output /tmp/backup.json


    Cela va lire la configuration stockée dans `/path/to/old/config.ini` et
    exportera votre ancienne instance au format JSON spécifique à Kresus dans
    `/tmp/backup.json`.

- depuis Kresus, utilisez l'outil d'import en passant le fichier JSON exporté
  auparavant pour réimporter toutes vos données.

## Mettre en place les notifications avec Apprise

Pour que les utilisateur.ice.s de votre instance aient accès aux notifications
via [Apprise](https://github.com/caronc/apprise/), il est nécessaire de définir
un [serveur d'API Apprise](https://github.com/caronc/apprise-api). Dans votre
fichier de configuration `config.ini`, remplissez la ligne suivante :

    :::INI
    appriseApiBaseUrl=http://localhost:8000/

## Recommandations pare-feu

Vous devrez définir les autorisations suivantes :

- Accès http/https au site web de votre banque, pour récupérer les nouvelles
  opérations.
- Accès http/https aux dépôts Woob, pour la mise à jour automatique des modules
  avant les rappatriements automatiques, si vous utilisez la fonctionnalité de
  mise à jour automatique.

## Backup / restauration d'une instance

Pour ces deux processus, il est fortement conseillé d'éteindre Kresus, pour
éviter que les données ne soient modifiées alors qu'elles sont en train d'être
copiées.

### Backup

- Sauvegardez votre fichier de configuration.
- Sauvegardez l'intégralité du contenu du répertoire de données `datadir` de
  Kresus (tel que renseigné dans le fichier de configuration).
- Après la version 0.16 incluse, sauvegardez le contenu de votre base de
  données avec un outil adéquat, ou en sauvegardant les fichiers de données
  associés à la base en question. Par exemple, vous pouvez utiliser `pg_dump`
  pour PostgreSQL, qui exportera la base voulue vers un fichier SQL avec toutes
  les données en clair.

### Restauration

Le processus de restauration consiste simplement en la restauration de tous les
fichiers sauvegardés lors de l'étape de backup.
