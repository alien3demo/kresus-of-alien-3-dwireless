Title: Kresus 0.15
Date: 2019-11-09 21:00
Lang: fr
Author: Les mainteneurs de Kresus
Slug: kresus-version-0-15-0

Cela fait 9 mois et 219 commits depuis la version précédente, il est donc temps
de sortir une nouvelle version de Kresus. Voici donc la version 0.15 !

Si vous êtes pressés et ne désirez que l'essentiel, nous vous invitons à lire
au moins la section "Le point administration système" pour connaître les
nouveaux besoins de Kresus !

## Le point sysadmin

### Nodejs 12

La version de NodeJS prise en charge passe à la 12 (supportée à long terme à
l'heure où ce billet est écrit). Cela ne veut pas dire que Kresus ne
fonctionnera pas sur les versions précédentes, mais que le support ne sera pas
officiellement assuré.

### Docker : prise en charge de python 3

Le support de python 2 prenant fin en janvier 2020, il est grand temps de
migrer vers python 3. En ce qui concerne Kresus les deux versions sont prises
en charge pour l'instant, mais on migre les outils et scripts progressivement.
C'est le cas de toutes nos images Docker qui se basent désormais sur python 3.
Pour les prochaines versions de Kresus, la version 3.5 de Python sera
nécessaire a minima, et le support Python 2 ne sera plus officiellement assuré.

### Configuration d'Apache

La communauté a continué d'améliorer la documentation des serveurs Web reverses
proxies en ajoutant une [configuration pour
Apache](https://framagit.org/kresusapp/kresus/blob/master/support/apache/apache.conf).
Merci à @ChristopheHenry pour cette contribution !

### Weboob

La version de [Weboob](http://weboob.org/) nécessaire est désormais la dernière
version stable 1.5, à l'heure où on écrit ce ticket. Weboob passera bientôt à
la 1.6, compatible uniquement avec python 3.

## Nouvelles fonctionnalités

### Affichage de la balance totale par devise dans le menu de banques

Auparavant, Kresus n'était pas capable d'afficher la balance totale d'un accès
bancaire si les comptes utilisaient des devises différentes. Désormais, le
total par devise est affiché à la place. Merci @sinopsysHK pour cette
contribution !

### Icône d'alerte en cas d'erreur de connexion à votre banque

Lorsque Kresus récupère vos nouvelles opérations chaque nuit depuis le site de
votre banque, il est possible qu'une erreur survienne : le site est en
maintenance, en panne, ou encore a changé et l'outil qui se connecte au site et
récupère les données ne le reconnaît plus. Jusqu'ici vous n'étiez pas mis⋅e au
courant de cette erreur au sein de l'interface : seule la date de dernière
synchronisation restait figée.  Désormais une icône d'alerte s'affiche à côte
du nom de la banque pour vous prévenir qu'un souci a eu lieu.

![Capture d'écran de l'icône d'alerte]({static}/images/blog/015-connection-status.png)

### Import OFX

Vous souhaitez peut-être utiliser Kresus mais avez peur de perdre l'historique
de vos transactions déjà saisies et catégorisées avec amour dans un autre
logiciel. Si ce logiciel permet un export au format OFX, Kresus propose
maintenant d'importer ce format. Si ce logiciel ne permet pas d'export au
format OFX,  il reste deux possibilités : vous trouvez un logiciel qui permet
de convertir ce format vers OFX (ex: QIF vers OFX) ou enfin si vous vous sentez
brave et bricoleur⋅se, nous avons documenté le format que Kresus reconnaît :
https://kresus.org/user-doc.html#puis-je-importer-de-lofx-ou-dautres-formats_1,
il s'agira ensuite de convertir votre format vers ce dernier.

![Capture d'écran du formulaire d'import]({static}/images/blog/015-ofx-import.png)

### Support des champs optionnels

Certaines banques demandent des champs supplémentaires lors de
l'authentification selon que vous êtes un⋅e client⋅e particulier⋅ère ou
professionnel⋅le (par exemple, un code PIN). Jusque là Kresus ignorait ces
champs optionnels, rendant l'authentification impossible pour un compte
professionnel. Cette nouvelle version corrige donc le problème et ouvre le
support à de tels comptes.

### Affichage de la date dans la modale d'une opération

La date de réalisation de vos opérations s'invite dans la modale présentant les
détails d'une opération, disponible en cliquant sur le "+" à gauche de la
ligne, ou par un appui long sur la ligne sur mobile.

### Notifications

Des notifications, ou "toasts" apparaissent désormais en haut à droite de votre
écran lorsque vous effectuez une action (ex: mise à jour d'un accès, mise à
jour des modules weboob) pour vous signifier la bonne réalisation ou l'échec de
celle-ci.

![Capture d'écran d'une notification d'erreur]({static}/images/blog/015-toast.png)

### Support du débit différé et affichage de l'encours

Kresus supporte désormais les cartes à débit différé, et affiche le total des
opérations futures (encours) séparément des opérations passées. Cette partie
étant encore relativement jeune et testée sur un petit nombre de banques, il se
peut que le support ne soit pas encore idéal sur d'autres banques non testées.
Si vous voyez des bugs, n'hésitez-pas à nous contacter via notre forum !

### Mode de démonstration/bac à sable

Vous avez installé Kresus mais êtes encore un peu frileux⋅se à l'idée d'y
connecter vos comptes bancaires et souhaitez manipuler l'interface un peu avant
? Le mode démo est là pour vous : il va automatiquement créer des comptes et
transactions factices vous permettant de jouer en toute sécurité avec Kresus.
Une fois que vous avez fini de tester, il vous est possible de quitter le mode
démo pour pouvoir entrer vos vrais identifiants.

### Possibilité de définir un thème par navigateur

Le paramètre définissant le thème que vous préférez déménage de votre serveur
vers votre navigateur. En pratique ça signifie que vous pouvez définir un thème
sur votre téléphone et un autre sur votre ordinateur, par exemple le thème
*dark* sur téléphone et *light* sur votre ordinateur.

### Possibilité de rechercher sur plusieurs catégories

Si vous recherchez une opération mais n'êtes plus certain⋅e de la catégorie
dans laquelle vous l'avez affectée, Kresus vous permet de rechercher sur
plusieurs catégories. Fini les doutes entre « vacances » et « restaurants » si
vous allez au restau pendant vos vacances !

## Expérience utilisateur.ice

Plusieurs tentatives d'améliorations ont été faites sur l'interface, pour une
navigation et utilisation plus aisée.

Pour éviter la confusion il existe désormais deux menus : le menu à gauche
accessible depuis le menu "hamburger", qui vous permet d'accéder aux
différentes visualisations liées à un compte (relevé, graphiques, budgets…), et
un nouveau menu à droite accessible depuis une icône d'engrenage qui regroupe
tout ce qui est global à l'application : paramètres de weboob, journaux,
personnalisation de l'interface, gestion des accès bancaires…

![Capture d'écran du menu de préférences]({static}/images/blog/015-cog-menu.png)

La navigation mobile dans les préférences a été également améliorée. Il est
également possible de naviguer au clavier entre ces différents menus.

L'interface d'import a été revue et simplifiée, ne vous demandant notamment le
mot de passe d'import que si c'est nécessaire.

Côté graphiques, des messages d'aide vous aident à découvrir et comprendre ce
que représente chaque visualisation. Ces messages peuvent être désactivés si
vous n'en avez plus besoin, et vous pourrez toujours les ré-activer depuis les
paramètres. D'autres feront leur apparence dans les prochaines versions dans
d'autres parties de l'interface pour vous faciliter la prise en main de Kresus.

![Capture d'écran d'un message d'aide]({static}/images/blog/015-help-msg.png)

## Changements internes

- Beaucoup de préparations pour la migration vers un backend SQL et un monde
  multi-utilisateurs.
- Meilleure gestion des erreurs de mise à jour des modules weboob.
- Meilleure performance de la liste infinie.
- Séparation des fichiers CSS en plusieurs afin d'améliorer la maintenabilité.
- La récupération automatique des opérations essaie de minimiser la quantité de
  données récupérées, pour des synchronisations plus courtes.

## Prise en charge de nouvelles banques

- Humanis
- Themis banque
- Sogecarte Net
- Tickets CESU

## Merci !

Kresus ne pourrait exister sans ses contributeur.ice.s, merci à tout le monde !
Et merci d'avoir lu ce billet de blog jusqu'au bout !

Si vous avez des retours à nous faire, des suggestions ou des remarques,
n'hésitez-pas à nous le faire savoir, sur [notre
forum](https://community.kresus.org),
[mastodon](https://tutut.delire.party/@kresus),
[twitter](https://twitter.com/kresusapp), ou via Matrix (#kresus:delire.party)
ou notre [canal IRC](https://kiwiirc.com/client/chat.freenode.net/kresus) !
