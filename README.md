# Kresus.org website

This contains the source of the kresus.org website, which is propulsed by
Pelican.

## Basic how-to

Recreate the content in the `output/` directory:

```make html```

## How to add translations

1. Add `{% trans %}translatable strings{% endtrans %}` strings to the
   templates (HTML) files.
2. Run `make update_translations` to update the catalog of available
   translations.
3. Translate things in the
   `themes/custom/translations/en/LC_MESSAGES/messages.po` file.
4. Run `make compile_translations` to update the binary `.mo` file.
5. Make sure to check in the binary translated files.
